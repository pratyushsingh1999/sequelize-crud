const Sequelize = require("sequelize");
const sequelize = require("../database");

const User = sequelize.define("User", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true
    },
    name:{
        type: Sequelize.STRING(),
        allowNull: false
    },
    age:{
        type: Sequelize.INTEGER(),
        allowNull: false
    },
});

module.exports = User;
