const Sequelize = require("sequelize");

const config =require('./config')

const sequelize = new Sequelize(config.DATABASE_NAME, config.USERNAME, config.PASSWORD, {
    host: config.HOST,
    dialect: config.DIALECT,
    pool: {
        max: 10,
        min: 0,
        acquire: 3000,
        idle: 1000
    }
})
module.exports = sequelize;
