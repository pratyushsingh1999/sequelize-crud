const User = require('../models/user');

const createUser = () => {
    const user = {
        name: "Pratyush",
        email:"Pratyush@gmail.com"
    }
    User.create(user)
        .catch((error)=>{
            console.log(error)
        })
}

const findAllUser = () => {
    User.findAll()
        .then((data)=>{
            console.log(data)
        })
        .catch((error)=>{
            console.log(error)
        })
}

const findUserById = () => {
    const id = 5
    User.findByPk(id)
        .then((data)=>{
            if(data == null){
                console.log(`User not found`)
            }
            else{
                console.log(data)
            }
        })
        .catch((error)=>{
            console.log(error)
        })
}

const updateUserById = () => {
    const id = 1
    const user = {
        email:"pratyushSingh@gmail.com"
    }

    User.update(user, {
        where: {
            id: id
        }
    })
        .then((data)=>{
            if(data == 1){
                console.log("User Updated")
            }
            else{
                console.log(`Updation failed`)
            }
        })
        .catch((error)=>{
            console.log(error)
        })
}

// Delete a row with the specified id in the request
const removeUserById = () => {
    const id = 2

    User.destroy({
        where:{
            id: id
        }
    })
        .then((data)=>{
            if(data == 1){
                console.log("User deleted")
            }
            else{
                console.log("Deletion Failed")
            }
        })
        .catch((error)=>{
            console.log(error)
        })
}

const removeAllUsers = () => {

    User.destroy({
        where: {},
        truncate : false
    })
        .then((data)=>{
            console.log("Deleted all users")
        })
        .catch((error)=>{
            console.log(error)
        })
}


module.exports = {
    createUser,
    findAllUser,
    findUserById,
    updateUserById,
    removeUserById,
    removeAllUsers,
}
